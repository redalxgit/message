package com.ms.server.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.ms.server.interceptor.AuthSecurityInterceptor;
import com.ms.server.interceptor.UserSecurityInterceptor;

/**
 * WebMvc的配置
 * @author yuejing
 * @date 2019年3月5日 下午1:15:08
 */
@Configuration
//@EnableWebMvc
public class WebMvcConfig implements WebMvcConfigurer {
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// 添加拦截器
		UserSecurityInterceptor userSecurityInterceptor = new UserSecurityInterceptor();
		userSecurityInterceptor.setLoginUrl("/index.jsp");
		registry.addInterceptor(userSecurityInterceptor)
		.addPathPatterns("/*/f-view/**", "/*/f-json/**");
		
		registry.addInterceptor(new AuthSecurityInterceptor())
		.addPathPatterns("/*/*");
		WebMvcConfigurer.super.addInterceptors(registry);
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
		registry.addResourceHandler("/view/**").addResourceLocations("/view/");
		WebMvcConfigurer.super.addResourceHandlers(registry);
	}
	
	@Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        //setUseSuffixPatternMatch 后缀模式匹配
        configurer.setUseSuffixPatternMatch(true);
        //setUseTrailingSlashMatch 自动后缀路径模式匹配
        configurer.setUseTrailingSlashMatch(true);
    }
}
