package com.message.ui.msg.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.message.admin.sys.pojo.SysInfo;
import com.message.admin.sys.service.SysInfoService;
import com.message.ui.comm.controller.BaseController;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * sysInfo的Controller
 * @author autoCode
 * @date 2017-12-27 15:24:04
 * @version V1.0.0
 */
@Controller
public class SysInfoUiController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SysInfoUiController.class);

	@Autowired
	private SysInfoService sysInfoService;
	
	@RequestMapping(value = "/sysInfo/f-view/manager")
	public String manger(HttpServletRequest request, ModelMap modelMap) {
		return "admin/sys-info/sysInfo-manager";
	}

	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/sysInfo/f-json/pageQuery")
	@ResponseBody
	public void pageQuery(HttpServletRequest request, HttpServletResponse response, SysInfo sysInfo) {
		ResponseFrame frame = null;
		try {
			frame = sysInfoService.pageQuery(sysInfo);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
		}
		writerJson(response, frame);
	}

	@RequestMapping(value = "/sysInfo/f-view/edit")
	public String edit(HttpServletRequest request, ModelMap modelMap, String sysNo) {
		if(FrameStringUtil.isNotEmpty(sysNo)) {
			modelMap.put("sysInfo", sysInfoService.get(sysNo));
		}
		return "admin/sys-info/sysInfo-edit";
	}
	
	@RequestMapping(value = "/sysInfo/f-json/save")
	@ResponseBody
	public void save(HttpServletRequest request, HttpServletResponse response, SysInfo sysInfo) {
		ResponseFrame frame = null;
		try {
			frame = sysInfoService.saveOrUpdate(sysInfo);
		} catch (Exception e) {
			LOGGER.error("保存异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
	
	@RequestMapping(value = "/sysInfo/f-json/delete")
	@ResponseBody
	public void delete(HttpServletRequest request, HttpServletResponse response, String sysNo) {
		ResponseFrame frame = null;
		try {
			frame = sysInfoService.delete(sysNo);
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
}