package com.message.ui.msg.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.message.admin.msg.pojo.MsgGroup;
import com.message.admin.msg.service.MsgGroupService;
import com.message.admin.sys.service.SysInfoService;
import com.message.ui.comm.controller.BaseController;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 系统分组的Controller
 * @author autoCode
 * @date 2017-12-27 15:24:04
 * @version V1.0.0
 */
@Controller
public class MsgGroupUiController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MsgGroupUiController.class);

	@Autowired
	private MsgGroupService msgGroupService;
	@Autowired
	private SysInfoService sysInfoService;
	
	@RequestMapping(value = "/msgGroup/f-view/manager")
	public String manger(HttpServletRequest request, ModelMap modelMap) {
		modelMap.put("sysInfos", sysInfoService.findKvAll());
		return "admin/sys-info/msgGroup-manager";
	}

	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/msgGroup/f-json/pageQuery")
	@ResponseBody
	public void pageQuery(HttpServletRequest request, HttpServletResponse response, MsgGroup msgGroup) {
		ResponseFrame frame = null;
		try {
			frame = msgGroupService.pageQuery(msgGroup);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
		}
		writerJson(response, frame);
	}

	@RequestMapping(value = "/msgGroup/f-view/edit")
	public String edit(HttpServletRequest request, ModelMap modelMap, String id, String sysNo) {
		if(FrameStringUtil.isNotEmpty(id)) {
			modelMap.put("msgGroup", msgGroupService.get(id, sysNo));
		}
		modelMap.put("sysInfos", sysInfoService.findKvAll());
		return "admin/sys-info/msgGroup-edit";
	}
	
	@RequestMapping(value = "/msgGroup/f-json/save")
	@ResponseBody
	public void save(HttpServletRequest request, HttpServletResponse response, MsgGroup msgGroup) {
		ResponseFrame frame = null;
		try {
			frame = msgGroupService.saveOrUpdate(msgGroup);
		} catch (Exception e) {
			LOGGER.error("保存异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
	
	@RequestMapping(value = "/msgGroup/f-json/delete")
	@ResponseBody
	public void delete(HttpServletRequest request, HttpServletResponse response, String id) {
		ResponseFrame frame = null;
		try {
			frame = msgGroupService.delete(id);
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
}