package com.message.ui.mot.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.message.admin.mot.pojo.MotPush;
import com.message.admin.mot.service.MotPushService;
import com.message.admin.sys.service.SysInfoService;
import com.message.ui.comm.controller.BaseController;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 推送事件的Controller
 * @author yuejing
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@Controller
public class MotPushUiController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MotPushUiController.class);

	@Autowired
	private MotPushService motPushService;
	@Autowired
	private SysInfoService sysInfoService;
	
	@RequestMapping(value = "/motPush/f-view/manager")
	public String manger(HttpServletRequest request, ModelMap modelMap) {
		modelMap.put("sysInfos", sysInfoService.findKvAll());
		return "admin/mot/push-manager";
	}

	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/motPush/f-json/pageQuery")
	@ResponseBody
	public void pageQuery(HttpServletRequest request, HttpServletResponse response, MotPush motPush) {
		ResponseFrame frame = null;
		try {
			frame = motPushService.pageQuery(motPush);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}

	@RequestMapping(value = "/motPush/f-view/edit")
	public String edit(HttpServletRequest request, ModelMap modelMap) {
		modelMap.put("sysInfos", sysInfoService.findKvAll());
		return "admin/mot/push-edit";
	}

	@RequestMapping(value = "/motPush/f-view/look")
	public String look(HttpServletRequest request, ModelMap modelMap, String mpId) {
		if(FrameStringUtil.isNotEmpty(mpId)) {
			MotPush motPush = motPushService.getDtl(mpId);
			modelMap.put("motPush", motPush);
		}
		modelMap.put("sysInfos", sysInfoService.findKvAll());
		return "admin/mot/push-look";
	}
	
	@RequestMapping(value = "/motPush/f-json/save")
	@ResponseBody
	public void save(HttpServletRequest request, HttpServletResponse response, MotPush motPush) {
		ResponseFrame frame = null;
		try {
			frame = motPushService.save(motPush);
		} catch (Exception e) {
			LOGGER.error("保存异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
	
	@RequestMapping(value = "/motPush/f-json/delete")
	@ResponseBody
	public void delete(HttpServletRequest request, HttpServletResponse response, String mpId) {
		ResponseFrame frame = null;
		try {
			frame = motPushService.delete(mpId);
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
}