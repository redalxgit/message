package com.message.admin.mot.service;

import org.springframework.stereotype.Component;

import com.message.admin.mot.pojo.MotPush;
import com.system.handle.model.ResponseFrame;

/**
 * mot_push的Service
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@Component
public interface MotPushService {
	
	/**
	 * 保存
	 * @param motPush
	 * @return
	 * @throws Exception 
	 */
	public ResponseFrame save(MotPush motPush) throws Exception;
	
	/**
	 * 根据mpId获取对象
	 * @param mpId
	 * @return
	 */
	public MotPush get(String mpId);

	/**
	 * 分页获取对象
	 * @param motPush
	 * @return
	 */
	public ResponseFrame pageQuery(MotPush motPush);
	
	/**
	 * 根据mpId删除对象
	 * @param mpId
	 * @return
	 */
	public ResponseFrame delete(String mpId);

	public MotPush getDtl(String mpId);
}