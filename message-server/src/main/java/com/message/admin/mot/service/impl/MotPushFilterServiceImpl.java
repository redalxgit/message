package com.message.admin.mot.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.message.admin.mot.dao.MotPushFilterDao;
import com.message.admin.mot.pojo.MotPushFilter;
import com.message.admin.mot.service.MotPushFilterService;
import com.system.comm.model.Page;
import com.system.comm.utils.FrameNoUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * mot_push_filter的Service
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@Component
public class MotPushFilterServiceImpl implements MotPushFilterService {

	@Autowired
	private MotPushFilterDao motPushFilterDao;
	
	@Override
	public ResponseFrame save(MotPushFilter motPushFilter) {
		ResponseFrame frame = new ResponseFrame();
		motPushFilter.setMpfId(FrameNoUtil.uuid());
		motPushFilterDao.save(motPushFilter);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public MotPushFilter get(String mpfId) {
		return motPushFilterDao.get(mpfId);
	}

	@Override
	public ResponseFrame pageQuery(MotPushFilter motPushFilter) {
		motPushFilter.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = motPushFilterDao.findMotPushFilterCount(motPushFilter);
		List<MotPushFilter> data = null;
		if(total > 0) {
			data = motPushFilterDao.findMotPushFilter(motPushFilter);
		}
		Page<MotPushFilter> page = new Page<MotPushFilter>(motPushFilter.getPage(), motPushFilter.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
	
	@Override
	public ResponseFrame delete(String mpfId) {
		ResponseFrame frame = new ResponseFrame();
		motPushFilterDao.delete(mpfId);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public void deleteByMpId(String mpId) {
		motPushFilterDao.deleteByMpId(mpId);
	}

	@Override
	public List<MotPushFilter> findByMpId(String mpId) {
		return motPushFilterDao.findByMpId(mpId);
	}
}