package com.message.admin.mot.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.message.admin.mot.dao.MotEventDao;
import com.message.admin.mot.enums.MotEventReqEncrypt;
import com.message.admin.mot.pojo.MotEvent;
import com.message.admin.mot.service.MotEventService;
import com.message.admin.mot.utils.MotPluginUtil;
import com.message.admin.sys.service.SysInfoService;
import com.system.comm.model.Page;
import com.system.comm.utils.FrameNoUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseFrame;
import com.system.handle.model.ResponseCode;

/**
 * mot_event的Service
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@Component
public class MotEventServiceImpl implements MotEventService {

	@Autowired
	private MotEventDao motEventDao;
	@Autowired
	private SysInfoService sysInfoService;
	
	@Override
	public ResponseFrame saveOrUpdate(MotEvent motEvent) {
		ResponseFrame frame = new ResponseFrame();
		if(FrameStringUtil.isEmpty(motEvent.getMeId())) {
			motEvent.setMeId(FrameNoUtil.uuid());
			motEventDao.save(motEvent);
		} else {
			motEventDao.update(motEvent);
		}
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public MotEvent get(String meId) {
		return motEventDao.get(meId);
	}

	@Override
	public ResponseFrame pageQuery(MotEvent motEvent) {
		motEvent.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = motEventDao.findMotEventCount(motEvent);
		List<MotEvent> data = null;
		if(total > 0) {
			data = motEventDao.findMotEvent(motEvent);
			for (MotEvent me : data) {
				me.setReqEncryptName(MotEventReqEncrypt.getText(me.getReqEncrypt()));
				me.setFilterPlugName(MotPluginUtil.getPluginName(me.getFilterPlug()));
				me.setSysName(sysInfoService.getName(me.getSysNo()));
			}
		}
		Page<MotEvent> page = new Page<MotEvent>(motEvent.getPage(), motEvent.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
	
	@Override
	public ResponseFrame delete(String meId) {
		ResponseFrame frame = new ResponseFrame();
		motEventDao.delete(meId);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public String getName(String meId) {
		MotEvent me = get(meId);
		return me == null ? null : me.getName();
	}

	@Override
	public List<MotEvent> findBySysNo(String sysNo) {
		return motEventDao.findBySysNo(sysNo);
	}
}