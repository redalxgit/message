package com.message.admin.mot.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.message.admin.mot.dao.MotSubFilterDao;
import com.message.admin.mot.pojo.MotSubFilter;
import com.message.admin.mot.service.MotSubFilterService;
import com.system.comm.model.Page;
import com.system.comm.utils.FrameNoUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseFrame;
import com.system.handle.model.ResponseCode;

/**
 * mot_sub_filter的Service
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@Component
public class MotSubFilterServiceImpl implements MotSubFilterService {

	@Autowired
	private MotSubFilterDao motSubFilterDao;
	
	@Override
	public ResponseFrame saveOrUpdate(MotSubFilter motSubFilter) {
		ResponseFrame frame = new ResponseFrame();
		if(FrameStringUtil.isEmpty(motSubFilter.getMsfId())) {
			motSubFilter.setMsfId(FrameNoUtil.uuid());
			motSubFilterDao.save(motSubFilter);
		} else {
			motSubFilterDao.update(motSubFilter);
		}
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public MotSubFilter get(String msfId) {
		return motSubFilterDao.get(msfId);
	}

	@Override
	public ResponseFrame pageQuery(MotSubFilter motSubFilter) {
		motSubFilter.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = motSubFilterDao.findMotSubFilterCount(motSubFilter);
		List<MotSubFilter> data = null;
		if(total > 0) {
			data = motSubFilterDao.findMotSubFilter(motSubFilter);
		}
		Page<MotSubFilter> page = new Page<MotSubFilter>(motSubFilter.getPage(), motSubFilter.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
	
	@Override
	public ResponseFrame delete(String msfId) {
		ResponseFrame frame = new ResponseFrame();
		motSubFilterDao.delete(msfId);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public void deleteByMsId(String msId) {
		motSubFilterDao.deleteByMsId(msId);
	}
}