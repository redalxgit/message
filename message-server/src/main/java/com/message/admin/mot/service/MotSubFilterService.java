package com.message.admin.mot.service;

import org.springframework.stereotype.Component;

import com.message.admin.mot.pojo.MotSubFilter;
import com.system.handle.model.ResponseFrame;

/**
 * mot_sub_filter的Service
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@Component
public interface MotSubFilterService {
	
	/**
	 * 保存或修改
	 * @param motSubFilter
	 * @return
	 */
	public ResponseFrame saveOrUpdate(MotSubFilter motSubFilter);
	
	/**
	 * 根据msfId获取对象
	 * @param msfId
	 * @return
	 */
	public MotSubFilter get(String msfId);

	/**
	 * 分页获取对象
	 * @param motSubFilter
	 * @return
	 */
	public ResponseFrame pageQuery(MotSubFilter motSubFilter);
	
	/**
	 * 根据msfId删除对象
	 * @param msfId
	 * @return
	 */
	public ResponseFrame delete(String msfId);

	public void deleteByMsId(String msId);
}