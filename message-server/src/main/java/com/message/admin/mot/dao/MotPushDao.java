package com.message.admin.mot.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.message.admin.mot.pojo.MotPush;

/**
 * mot_push的Dao
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
public interface MotPushDao {

	public abstract void save(MotPush motPush);

	public abstract void update(MotPush motPush);

	public abstract void delete(@Param("mpId")String mpId);

	public abstract MotPush get(@Param("mpId")String mpId);

	public abstract List<MotPush> findMotPush(MotPush motPush);
	
	public abstract int findMotPushCount(MotPush motPush);
}