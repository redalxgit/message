package com.message.admin.mot.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.message.admin.comm.controller.BaseRestController;
import com.message.admin.mot.pojo.MotSub;
import com.message.admin.mot.service.MotSubService;
import com.monitor.api.ApiInfo;
import com.monitor.api.ApiParam;
import com.monitor.api.ApiRes;
import com.system.comm.model.Orderby;
import com.system.comm.utils.FrameJsonUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * mot_sub的Controller
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@RestController
public class MotSubController extends BaseRestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MotSubController.class);

	@Autowired
	private MotSubService motSubService;
	
	@RequestMapping(name = "motSub-获取详细信息", value = "/motSub/get")
	@ApiInfo(params = {
			@ApiParam(name="msId", code="msId", value=""),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
			
	})
	public ResponseFrame get(String msId) {
		try {
			ResponseFrame frame = new ResponseFrame();
			frame.setBody(motSubService.get(msId));
			frame.setCode(ResponseCode.SUCC.getCode());
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}

	@RequestMapping(name = "motSub-新增或修改", value = "/motSub/saveOrUpdate")
	@ApiInfo(params = {
			@ApiParam(name="编号", code="msId", value=""),
			@ApiParam(name="来源系统编号", code="sysNo", value=""),
			@ApiParam(name="事件编号", code="meId", value=""),
			@ApiParam(name="订阅人", code="subUserId", value=""),
			@ApiParam(name="邮件通知号码[多个;分隔]", code="receEmail", value=""),
			@ApiParam(name="短信通知号码[多个;分隔]", code="receSms", value=""),
			@ApiParam(name="创建时间", code="createTime", value=""),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
	})
	public ResponseFrame saveOrUpdate(MotSub motSub) {
		try {
			ResponseFrame frame = motSubService.saveOrUpdate(motSub);
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}

	@RequestMapping(name = "motSub-分页查询信息", value = "/motSub/pageQuery")
	@ApiInfo(params = {
			@ApiParam(name="页面", code="page", value="1"),
			@ApiParam(name="每页大小", code="size", value="10"),
			@ApiParam(name="排序[{\"property\": \"createTime\", \"type\":\"desc\", \"order\":1}]", code="orderby", value="", required=false),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
			@ApiRes(name="当前页码", code="page", pCode="body", clazz=Integer.class, value="1"),
			@ApiRes(name="每页大小", code="size", pCode="body", clazz=Integer.class, value="10"),
			@ApiRes(name="总页数", code="totalPage", pCode="body", clazz=Integer.class, value="5"),
			@ApiRes(name="总记录数", code="total", pCode="body", clazz=Integer.class, value="36"),
			@ApiRes(name="数据集合", code="rows", pCode="body", clazz=List.class, value=""),
			@ApiRes(name="编号", code="msId", pCode="rows", value=""),
			@ApiRes(name="来源系统编号", code="sysNo", pCode="rows", value=""),
			@ApiRes(name="事件编号", code="meId", pCode="rows", value=""),
			@ApiRes(name="订阅人", code="subUserId", pCode="rows", value=""),
			@ApiRes(name="邮件通知号码[多个;分隔]", code="receEmail", pCode="rows", value=""),
			@ApiRes(name="短信通知号码[多个;分隔]", code="receSms", pCode="rows", value=""),
			@ApiRes(name="创建时间", code="createTime", pCode="rows", value=""),
	})
	public ResponseFrame pageQuery(MotSub motSub, String orderby) {
		try {
			if(FrameStringUtil.isNotEmpty(orderby)) {
				List<Orderby> orderbys = FrameJsonUtil.toList(orderby, Orderby.class);
				motSub.setOrderbys(orderbys);
			}
			ResponseFrame frame = motSubService.pageQuery(motSub);
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}

	@RequestMapping(name = "motSub-根据主键删除", value = "/motSub/delete")
	@ApiInfo(params = {
			@ApiParam(name="msId", code="msId", value=""),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
	})
	public ResponseFrame delete(String msId) {
		try {
			ResponseFrame frame = motSubService.delete(msId);
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}
}