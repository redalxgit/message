package com.message.admin.mot.plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.message.admin.mot.plugin.core.BaseMotPlugin;
import com.message.admin.mot.pojo.MotPush;
import com.message.admin.mot.pojo.MotSub;
import com.message.admin.mot.service.MotSubService;
import com.system.comm.utils.FrameJsonUtil;
import com.system.comm.utils.FrameSpringBeanUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseFrame;

/**
 * 接收人的插件<br>
 * 	不处理参数，推送的接收人和推送的接收邮箱和短信号码发送消息
 * push
 * 	receEmail: {receUserId:"t@qq.com;b@qq.com"}
 *	receSms: {receUserId:"13309874567;13398761234"}
 * @author yuejing
 * @date 2018年12月19日 上午10:00:22
 */
public class ReceMotPlugin extends BaseMotPlugin {

	@Override
	public ResponseFrame trigger(MotPush push) {
		ResponseFrame frame = new ResponseFrame();
		MotSubService motSubService = FrameSpringBeanUtil.getBean(MotSubService.class);
		List<MotSub> list = new ArrayList<MotSub>();
		// 指定了接收人
		List<String> receUserIds = FrameStringUtil.toArray(push.getReceUserIds(), ";");
		Map<String, String> receEmailMap = new HashMap<String, String>();
		if (FrameStringUtil.isNotEmpty(push.getReceEmail())) {
			// {receUserId:"t@qq.com;b@qq.com"}
			receEmailMap = FrameJsonUtil.toMap(push.getReceEmail());
		}
		Map<String, String> receSmsMap = new HashMap<String, String>();
		if (FrameStringUtil.isNotEmpty(push.getReceSms())) {
			// {receUserId:"13309874567;13398761234"}
			receSmsMap = FrameJsonUtil.toMap(push.getReceSms());
		}
		for (String uid : receUserIds) {
			MotSub ms = new MotSub();
			ms.setSysNo(push.getSysNo());
			ms.setMeId(push.getMeId());
			ms.setSubUserId(uid);
			ms.setReceEmail(receEmailMap.get(uid));
			ms.setReceSms(receSmsMap.get(uid));
			// 保存订阅
			motSubService.saveOrUpdate(ms);
			// 添加到发送列表
			list.add(ms);
		}
		
		// 发送消息
		sendMsg(push, list);
		frame.setSucc();
		return frame;
	}

}