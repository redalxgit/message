package com.message.admin.mot.utils;

import java.util.List;

import com.message.admin.mot.plugin.core.BaseMotPlugin;
import com.system.comm.model.KvEntity;

/**
 * MOT插件工具类
 * @author yuejing
 * @date 2018年12月19日 下午4:13:15
 */
public class MotPluginUtil {

	/**
     * 获取所有插件列表
     */
    public static List<KvEntity> getPlugins() {
        return BaseMotPlugin.getPlugins();
    }
    
    /**
     * 获取插件的名称
     * @param code
     * @return
     */
    public static String getPluginName(String code) {
    	List<KvEntity> list = getPlugins();
    	for (KvEntity kvEntity : list) {
			if (kvEntity.getCode().equals(code)) {
				return kvEntity.getValue();
			}
		}
    	return null;
    }
}
