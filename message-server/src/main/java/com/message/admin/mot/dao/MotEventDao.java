package com.message.admin.mot.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.message.admin.mot.pojo.MotEvent;

/**
 * mot_event的Dao
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
public interface MotEventDao {

	public abstract void save(MotEvent motEvent);

	public abstract void update(MotEvent motEvent);

	public abstract void delete(@Param("meId")String meId);

	public abstract MotEvent get(@Param("meId")String meId);

	public abstract List<MotEvent> findMotEvent(MotEvent motEvent);
	
	public abstract int findMotEventCount(MotEvent motEvent);

	public abstract List<MotEvent> findBySysNo(@Param("sysNo")String sysNo);
}