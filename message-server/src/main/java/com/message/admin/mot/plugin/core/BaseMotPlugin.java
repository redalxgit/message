package com.message.admin.mot.plugin.core;

import java.util.ArrayList;
import java.util.List;

import com.message.admin.mot.plugin.DefaultMotPlugin;
import com.message.admin.mot.plugin.ReceMotPlugin;
import com.message.admin.mot.plugin.TestMotPlugin;
import com.message.admin.mot.pojo.MotPush;
import com.message.admin.mot.pojo.MotPushRece;
import com.message.admin.mot.pojo.MotSub;
import com.message.admin.mot.service.MotPushReceService;
import com.message.admin.msg.enums.MsgInfoType;
import com.message.admin.msg.pojo.MsgInfo;
import com.message.admin.msg.service.MsgInfoService;
import com.system.comm.enums.Boolean;
import com.system.comm.model.KvEntity;
import com.system.comm.utils.FrameSpringBeanUtil;
import com.system.handle.model.ResponseFrame;

/**
 * MOT插件
 * @author yuejing
 * @date 2018年12月19日 上午10:00:45
 */
public abstract class BaseMotPlugin {

	private static List<KvEntity> plugins = new ArrayList<KvEntity>();
	static {
		plugins.add(new KvEntity(DefaultMotPlugin.class.getName(), "默认消息匹配插件(推送的接收人和订阅人一致时才能接收消息)"));
		plugins.add(new KvEntity(TestMotPlugin.class.getName(), "测试的插件(过滤条件:类型和字段1一致或订阅人的字段1为0时才能接收消息)"));
		plugins.add(new KvEntity(ReceMotPlugin.class.getName(), "接收人插件(推送的接收人和接收人对应的邮件手机号等才能接收消息)"));
		
		// TODO 这里扩展新写入的插件
	}
	/**
	 * 获取所有插件列表
	 * @return
	 */
	public static List<KvEntity> getPlugins() {
		return plugins;
	}
	
	/**
	 * 触发插件事件
	 * @param push
	 * @return
	 */
	public abstract ResponseFrame trigger(MotPush push);
	
	public void sendMsg(MotPush push, List<MotSub> list) {
		MotPushReceService motPushReceService = FrameSpringBeanUtil.getBean(MotPushReceService.class);
		MsgInfoService msgInfoService = FrameSpringBeanUtil.getBean(MsgInfoService.class);
		for (MotSub motSub : list) {
			MsgInfo msgInfo = new MsgInfo();
			msgInfo.setSysNo(push.getSysNo());
			msgInfo.setGroupId(msgInfo.getSysNo());
			msgInfo.setType(MsgInfoType.READ.getCode());
			msgInfo.setTitle(push.getTitle());
			msgInfo.setContent(push.getContent());
			msgInfo.setSendUserId(motSub.getSubUserId());
			msgInfo.setReceUserIds(motSub.getSubUserId());
			msgInfo.setExt1(null);
			msgInfo.setExt2(null);
			msgInfo.setExt3(null);
			msgInfo.setIsOpenCheck(Boolean.FALSE.getCode());
			
			msgInfo.setReceContent(push.getEmailContent());
			msgInfo.setReceEmailFiles(push.getEmailFiles());
			msgInfo.setSmsContent(push.getSmsContent());
			msgInfo.setReceEmails(motSub.getReceEmail());
			msgInfo.setRecePhones(motSub.getReceSms());
			msgInfoService.save(msgInfo);
			
			motPushReceService.save(new MotPushRece(push.getMpId(), motSub.getMsId(), msgInfo.getId()));
		}
	}
}
