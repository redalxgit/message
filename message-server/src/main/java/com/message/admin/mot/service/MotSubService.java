package com.message.admin.mot.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.message.admin.mot.pojo.MotSub;
import com.system.handle.model.ResponseFrame;

/**
 * mot_sub的Service
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@Component
public interface MotSubService {
	
	/**
	 * 保存或修改
	 * @param motSub
	 * @return
	 */
	public ResponseFrame saveOrUpdate(MotSub motSub);
	
	/**
	 * 根据msId获取对象
	 * @param msId
	 * @return
	 */
	public MotSub get(String msId);

	/**
	 * 分页获取对象
	 * @param motSub
	 * @return
	 */
	public ResponseFrame pageQuery(MotSub motSub);
	
	/**
	 * 根据msId删除对象
	 * @param msId
	 * @return
	 */
	public ResponseFrame delete(String msId);

	public List<MotSub> findByMeId(String meId);
	/**
	 * 根据时间ID和订阅人获取对象
	 * @param meId
	 * @param subUserId
	 * @return
	 */
	public MotSub getByMeIdSubuid(String meId, String subUserId);
}