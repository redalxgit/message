package com.message.admin.mot.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.message.admin.mot.pojo.MotPushRece;
import com.system.handle.model.ResponseFrame;

/**
 * mot_push_rece的Service
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@Component
public interface MotPushReceService {
	
	/**
	 * 保存
	 * @param motPushRece
	 * @return
	 */
	public ResponseFrame save(MotPushRece motPushRece);
	
	/**
	 * 根据mpId获取对象
	 * @param mpId
	 * @param msId
	 * @return
	 */
	public MotPushRece get(String mpId, String msId);

	/**
	 * 分页获取对象
	 * @param motPushRece
	 * @return
	 */
	public ResponseFrame pageQuery(MotPushRece motPushRece);
	
	/**
	 * 根据mpId删除对象
	 * @param mpId
	 * @return
	 */
	public ResponseFrame delete(String mpId);
	/**
	 * 根据推送编号获取实际的接收人列表
	 * @param mpId
	 * @return
	 */
	public List<MotPushRece> findByMpId(String mpId);
}