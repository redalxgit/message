package com.message.admin.mot.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.message.admin.mot.dao.MotPushDao;
import com.message.admin.mot.plugin.core.BaseMotPlugin;
import com.message.admin.mot.pojo.MotEvent;
import com.message.admin.mot.pojo.MotPush;
import com.message.admin.mot.pojo.MotPushFilter;
import com.message.admin.mot.service.MotEventService;
import com.message.admin.mot.service.MotPushFilterService;
import com.message.admin.mot.service.MotPushReceService;
import com.message.admin.mot.service.MotPushService;
import com.message.admin.sys.service.SysInfoService;
import com.system.comm.model.Page;
import com.system.comm.utils.FrameJsonUtil;
import com.system.comm.utils.FrameNoUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * mot_push的Service
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@Component
public class MotPushServiceImpl implements MotPushService {

	@Autowired
	private MotPushDao motPushDao;
	@Autowired
	private SysInfoService sysInfoService;
	@Autowired
	private MotPushFilterService motPushFilterService;
	@Autowired
	private MotEventService motEventService;
	@Autowired
	private MotPushReceService motPushReceService;
	
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
	public ResponseFrame save(MotPush motPush) {
		ResponseFrame frame = new ResponseFrame();
		if (FrameStringUtil.isEmpty(motPush.getContent())) {
			motPush.setContent(motPush.getTitle());
		}
		if (FrameStringUtil.isEmpty(motPush.getEmailContent())) {
			motPush.setEmailContent(motPush.getContent());
		}
		if (FrameStringUtil.isEmpty(motPush.getSmsContent())) {
			motPush.setSmsContent(motPush.getContent().length() > 140 ? motPush.getContent().substring(0, 140) : motPush.getContent());
		}
		motPush.setMpId(FrameNoUtil.uuid());
		motPushDao.save(motPush);
		
		if (FrameStringUtil.isNotEmpty(motPush.getFilterString())) {
			List<MotPushFilter> filters = FrameJsonUtil.toList(motPush.getFilterString(), MotPushFilter.class);
			motPush.setFilters(filters);
			for (MotPushFilter mpf : filters) {
				mpf.setMpId(motPush.getMpId());
				motPushFilterService.save(mpf);
			}
		}
		MotEvent me = motEventService.get(motPush.getMeId());
		// 调用插件发送消息
		BaseMotPlugin plugin = null;
		try {
			plugin = (BaseMotPlugin) Class.forName(me.getFilterPlug()).newInstance();
		} catch (Exception e) {
		}
		plugin.trigger(motPush);
		
		frame.setSucc();
		return frame;
	}

	@Override
	public MotPush get(String mpId) {
		return motPushDao.get(mpId);
	}

	@Override
	public ResponseFrame pageQuery(MotPush motPush) {
		motPush.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = motPushDao.findMotPushCount(motPush);
		List<MotPush> data = null;
		if(total > 0) {
			data = motPushDao.findMotPush(motPush);
			for (MotPush mp : data) {
				mp.setSysName(sysInfoService.getName(mp.getSysNo()));
				mp.setEventName(motEventService.getName(mp.getMeId()));
			}
		}
		Page<MotPush> page = new Page<MotPush>(motPush.getPage(), motPush.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
	
	@Override
	public ResponseFrame delete(String mpId) {
		ResponseFrame frame = new ResponseFrame();
		motPushDao.delete(mpId);
		motPushFilterService.deleteByMpId(mpId);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public MotPush getDtl(String mpId) {
		MotPush mp = get(mpId);
		if (mp != null) {
			mp.setFilters(motPushFilterService.findByMpId(mpId));
			mp.setSysName(sysInfoService.getName(mp.getSysNo()));
			mp.setEventName(motEventService.getName(mp.getMeId()));
			mp.setReces(motPushReceService.findByMpId(mp.getMpId()));
		}
		return mp;
	}
}