package com.message.admin.mot.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.message.admin.mot.pojo.MotEvent;
import com.system.handle.model.ResponseFrame;

/**
 * mot_event的Service
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@Component
public interface MotEventService {
	
	/**
	 * 保存或修改
	 * @param motEvent
	 * @return
	 */
	public ResponseFrame saveOrUpdate(MotEvent motEvent);
	
	/**
	 * 根据meId获取对象
	 * @param meId
	 * @return
	 */
	public MotEvent get(String meId);

	/**
	 * 分页获取对象
	 * @param motEvent
	 * @return
	 */
	public ResponseFrame pageQuery(MotEvent motEvent);
	
	/**
	 * 根据meId删除对象
	 * @param meId
	 * @return
	 */
	public ResponseFrame delete(String meId);

	public String getName(String meId);

	public List<MotEvent> findBySysNo(String sysNo);
}