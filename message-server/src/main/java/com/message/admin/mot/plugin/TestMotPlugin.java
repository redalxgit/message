package com.message.admin.mot.plugin;

import java.util.ArrayList;
import java.util.List;

import com.message.admin.mot.plugin.core.BaseMotPlugin;
import com.message.admin.mot.pojo.MotPush;
import com.message.admin.mot.pojo.MotPushFilter;
import com.message.admin.mot.pojo.MotSub;
import com.system.dao.BaseDao;
import com.system.handle.model.ResponseFrame;

public class TestMotPlugin extends BaseMotPlugin {

	@Override
	public ResponseFrame trigger(MotPush push) {
		ResponseFrame frame = new ResponseFrame();
		List<MotSub> list = new ArrayList<MotSub>();
		// 根据过滤条件筛选
		List<MotPushFilter> filters = push.getFilters();
		if (filters != null && filters.size() > 0) {
			// 只处理存在过滤条件的情况
			list.addAll(getReceList(push));
		}
		// 发送消息
		sendMsg(push, list);
		frame.setSucc();
		return frame;
	}

	private List<MotSub> getReceList(MotPush push) {
		BaseDao baseDao = new BaseDao();
		// 获取参入过滤条件类型一致和字段1一致，如果订阅人的字段1为0也可接收消息
		String sql = "select ms.ms_id msId,ms.sys_no sysNo,ms.me_id meId,ms.sub_user_id subUserId,ms.rece_email receEmail,ms.rece_sms receSms,ms.create_time createTime \r\n"
				+ "from mot_push_filter mpf inner join mot_push mp on(mp.mp_id=mpf.mp_id)\r\n" + 
				"inner join mot_sub ms on(ms.me_id=mp.me_id)\r\n" + 
				"inner join mot_sub_filter msf on(msf.ms_id=ms.ms_id)\r\n" + 
				"where msf.type=mpf.type and (msf.field1=0 or msf.field1=mpf.field1) and mp.mp_id=?";
		List<MotSub> list = baseDao.query(sql, MotSub.class, push.getMpId());
		return list;
	}
}