package com.message.admin.mot.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.message.admin.comm.controller.BaseRestController;
import com.message.admin.mot.pojo.MotPushFilter;
import com.message.admin.mot.service.MotPushFilterService;
import com.monitor.api.ApiInfo;
import com.monitor.api.ApiParam;
import com.monitor.api.ApiRes;
import com.system.comm.model.Orderby;
import com.system.comm.utils.FrameJsonUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * mot_push_filter的Controller
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@RestController
public class MotPushFilterController extends BaseRestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MotPushFilterController.class);

	@Autowired
	private MotPushFilterService motPushFilterService;
	
	@RequestMapping(name = "motPushFilter-获取详细信息", value = "/motPushFilter/get")
	@ApiInfo(params = {
			@ApiParam(name="mpfId", code="mpfId", value=""),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
			
	})
	public ResponseFrame get(String mpfId) {
		try {
			ResponseFrame frame = new ResponseFrame();
			frame.setBody(motPushFilterService.get(mpfId));
			frame.setCode(ResponseCode.SUCC.getCode());
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}

	@RequestMapping(name = "motPushFilter-分页查询信息", value = "/motPushFilter/pageQuery")
	@ApiInfo(params = {
			@ApiParam(name="页面", code="page", value="1"),
			@ApiParam(name="每页大小", code="size", value="10"),
			@ApiParam(name="排序[{\"property\": \"createTime\", \"type\":\"desc\", \"order\":1}]", code="orderby", value="", required=false),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
			@ApiRes(name="当前页码", code="page", pCode="body", clazz=Integer.class, value="1"),
			@ApiRes(name="每页大小", code="size", pCode="body", clazz=Integer.class, value="10"),
			@ApiRes(name="总页数", code="totalPage", pCode="body", clazz=Integer.class, value="5"),
			@ApiRes(name="总记录数", code="total", pCode="body", clazz=Integer.class, value="36"),
			@ApiRes(name="数据集合", code="rows", pCode="body", clazz=List.class, value=""),
			@ApiRes(name="编号", code="mpfId", pCode="rows", value=""),
			@ApiRes(name="推送事件编号", code="mpId", pCode="rows", value=""),
			@ApiRes(name="类型", code="type", pCode="rows", value=""),
			@ApiRes(name="字段1", code="field1", pCode="rows", value=""),
			@ApiRes(name="创建时间", code="createTime", pCode="rows", value=""),
			@ApiRes(name="字段2", code="field2", pCode="rows", value=""),
			@ApiRes(name="字段3", code="field3", pCode="rows", value=""),
			@ApiRes(name="字段4", code="field4", pCode="rows", value=""),
			@ApiRes(name="字段5", code="field5", pCode="rows", value=""),
			@ApiRes(name="字段6", code="field6", pCode="rows", value=""),
			@ApiRes(name="字段7", code="field7", pCode="rows", value=""),
			@ApiRes(name="字段8", code="field8", pCode="rows", value=""),
	})
	public ResponseFrame pageQuery(MotPushFilter motPushFilter, String orderby) {
		try {
			if(FrameStringUtil.isNotEmpty(orderby)) {
				List<Orderby> orderbys = FrameJsonUtil.toList(orderby, Orderby.class);
				motPushFilter.setOrderbys(orderbys);
			}
			ResponseFrame frame = motPushFilterService.pageQuery(motPushFilter);
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}

	@RequestMapping(name = "motPushFilter-根据主键删除", value = "/motPushFilter/delete")
	@ApiInfo(params = {
			@ApiParam(name="mpfId", code="mpfId", value=""),
	}, response = {
			@ApiRes(name="响应码[0成功、-1失败]", code="code", clazz=String.class, value="0"),
			@ApiRes(name="响应消息", code="message", clazz=String.class, value="success"),
			@ApiRes(name="主体内容", code="body", clazz=Object.class, value=""),
	})
	public ResponseFrame delete(String mpfId) {
		try {
			ResponseFrame frame = motPushFilterService.delete(mpfId);
			return frame;
		} catch (Exception e) {
			LOGGER.error("处理业务异常: " + e.getMessage(), e);
			return new ResponseFrame(ResponseCode.SERVER_ERROR);
		}
	}
}