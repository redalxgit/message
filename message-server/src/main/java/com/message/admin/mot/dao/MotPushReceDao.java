package com.message.admin.mot.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.message.admin.mot.pojo.MotPushRece;

/**
 * mot_push_rece的Dao
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
public interface MotPushReceDao {

	public abstract void save(MotPushRece motPushRece);

	public abstract void update(MotPushRece motPushRece);

	public abstract void delete(@Param("mpId")String mpId);

	public abstract MotPushRece get(@Param("mpId")String mpId, @Param("msId")String msId);

	public abstract List<MotPushRece> findMotPushRece(MotPushRece motPushRece);
	
	public abstract int findMotPushReceCount(MotPushRece motPushRece);

	public abstract List<MotPushRece> findByMpId(@Param("mpId")String mpId);
}