package com.message.admin.mot.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.message.admin.mot.pojo.MotSub;

/**
 * mot_sub的Dao
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
public interface MotSubDao {

	public abstract void save(MotSub motSub);

	public abstract void update(MotSub motSub);

	public abstract void delete(@Param("msId")String msId);

	public abstract MotSub get(@Param("msId")String msId);

	public abstract List<MotSub> findMotSub(MotSub motSub);
	
	public abstract int findMotSubCount(MotSub motSub);

	public abstract List<MotSub> findByMeId(@Param("meId")String meId);

	public abstract MotSub getByMeIdSubuid(@Param("meId")String meId, @Param("subUserId")String subUserId);

}