package com.message.admin.mot.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.message.admin.mot.pojo.MotPushFilter;
import com.system.handle.model.ResponseFrame;

/**
 * mot_push_filter的Service
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
@Component
public interface MotPushFilterService {
	
	/**
	 * 保存
	 * @param motPushFilter
	 * @return
	 */
	public ResponseFrame save(MotPushFilter motPushFilter);
	
	/**
	 * 根据mpfId获取对象
	 * @param mpfId
	 * @return
	 */
	public MotPushFilter get(String mpfId);

	/**
	 * 分页获取对象
	 * @param motPushFilter
	 * @return
	 */
	public ResponseFrame pageQuery(MotPushFilter motPushFilter);
	
	/**
	 * 根据mpfId删除对象
	 * @param mpfId
	 * @return
	 */
	public ResponseFrame delete(String mpfId);

	public void deleteByMpId(String mpId);

	public List<MotPushFilter> findByMpId(String mpId);
}