package com.message.admin.mot.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.message.admin.mot.pojo.MotSubFilter;

/**
 * mot_sub_filter的Dao
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
public interface MotSubFilterDao {

	public abstract void save(MotSubFilter motSubFilter);

	public abstract void update(MotSubFilter motSubFilter);

	public abstract void delete(@Param("msfId")String msfId);

	public abstract MotSubFilter get(@Param("msfId")String msfId);

	public abstract List<MotSubFilter> findMotSubFilter(MotSubFilter motSubFilter);
	
	public abstract int findMotSubFilterCount(MotSubFilter motSubFilter);

	public abstract void deleteByMsId(@Param("msId")String msId);
}