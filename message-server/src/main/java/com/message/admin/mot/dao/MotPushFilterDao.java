package com.message.admin.mot.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.message.admin.mot.pojo.MotPushFilter;

/**
 * mot_push_filter的Dao
 * @author autoCode
 * @date 2018-12-19 11:02:40
 * @version V1.0.0
 */
public interface MotPushFilterDao {

	public abstract void save(MotPushFilter motPushFilter);

	public abstract void update(MotPushFilter motPushFilter);

	public abstract void delete(@Param("mpfId")String mpfId);

	public abstract MotPushFilter get(@Param("mpfId")String mpfId);

	public abstract List<MotPushFilter> findMotPushFilter(MotPushFilter motPushFilter);
	
	public abstract int findMotPushFilterCount(MotPushFilter motPushFilter);

	public abstract void deleteByMpId(@Param("mpId")String mpId);

	public abstract List<MotPushFilter> findByMpId(@Param("mpId")String mpId);
}