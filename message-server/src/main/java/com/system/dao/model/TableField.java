package com.system.dao.model;

/**
 * 表属性
 * @author yuejing
 * @date 2018年12月18日 下午3:06:44
 */
public class TableField {
	// 属性名称
	private String name;
	// 属性数据类型
	private String type;
	// 显示大小
	private Integer displaySize;
	
	public TableField() {
		super();
	}
	public TableField(String name, String type, Integer displaySize) {
		super();
		this.name = name;
		this.type = type;
		this.displaySize = displaySize;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getDisplaySize() {
		return displaySize;
	}
	public void setDisplaySize(Integer displaySize) {
		this.displaySize = displaySize;
	}
}
