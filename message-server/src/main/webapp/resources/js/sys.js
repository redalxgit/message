JUtil.sys = {
		//跳转到指定地址
		location : function(url, islogin, isback) {
			if(islogin && user.uname==='') { JUtil.open.login(); return; }
			if(isback) {
				if(url.indexOf('?')!=-1) url += '&';
				else url += '?';
				url += 'back='+location;
			}
			location = url;
		},
		//初始化系统资料
		init : function() {
			if(config.initHmId)
				JUtil.sys.initHeader(config.initHmId);
		},
		//初始化header的选中
		initHeader : function(id) {
			$('#headerMenu>ul>li').each(function(i, obj) {
				if($(obj).attr('id') == id) {
					$(obj).addClass('active');
					return true;
				}
			});
		},
		initOptMore : function() {
			$('.opt-more').mouseover(function() {   
			    $(this).addClass('open');
			}).mouseout(function() {
				$(this).removeClass('open');
			});
		}
};

JUtil.biz = {
		
};

//表格操作
JUtil.biz.table = {
		add: function(tableId, suffixId) {
			var data = $('#' + tableId + 'DataTemp').html();
			if(suffixId == undefined) {
				suffixId = JUtil.date.getTime();
			}
			data = data.replace(/_idTemp/g, suffixId);
			var trId = tableId + 'Tr' + suffixId;
			$('#' + tableId + 'Tbody').append(['<tr id="',trId,'">',data,'</tr>'].join(''));
		},
		del: function(tableId, _this) {
			$(_this).parent().parent().parent().remove();
		},
		/*
		 * 验证
		 * required: 验证必填
		 * num>0: 验证输入的数值必须大于0
		 */
		validate: function(tableId) {
			var isValidate = true;
			var message = '';
			$('#' + tableId + 'Tbody').find('tr>td input').each(function(i, obj) {
				var validate = $(obj).attr('data-validate');
				if(!validate) {
					return true;
				}
				var value = $(obj).val();
				if(validate.indexOf('required')!=-1) {
					//验证是否输入内容
					if(JUtil.isEmpty(value)) {
						$(obj).focus();
						isValidate = false;
						message = '请输入表格中的必填数据';
						return false;
					}
				}
				if(validate.indexOf('num>0')!=-1) {
					//验证数值是否>0
					if(JUtil.isNotEmpty(value) && parseFloat(value)<=0) {
						$(obj).focus();
						isValidate = false;
						message = '输入的数字必须大于0';
						return false;
					}
				}
			});
			return {isValidate: isValidate, message: message};
		}
};
$(function() {
	JUtil.sys.initOptMore();
});