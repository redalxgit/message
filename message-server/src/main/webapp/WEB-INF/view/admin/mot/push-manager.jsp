<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="my" uri="/WEB-INF/tld/my.tld" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-推送事件</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body>
	<jsp:include page="/WEB-INF/view/inc/header.jsp"></jsp:include>
	<div class="container">
		<jsp:include page="/WEB-INF/view/admin/comm/left.jsp">
			<jsp:param name="first" value="mot"/>
			<jsp:param name="second" value="motPushManager"/>
		</jsp:include>
		<div class="c-right">
			<div class="panel panel-success">
				<div class="panel-heading panel-heading-tool">
					<div class="row">
						<div class="col-sm-5 title">MOT / <b>推送事件</b></div>
						<div class="col-sm-7 text-right">
							<div class="btn-group">
						  		<a href="javascript:location.reload()" class="btn btn-default btn-sm">刷新</a>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body">
				  	<div class="table-tool-panel">
						<div class="row">
							<div class="col-sm-6 enter-panel">
								<div class="btn-group">
								<my:select id="sysNo" items="${sysInfos}" headerKey="" headerValue="所有系统" cssCls="form-control input-sm w-120" value="" exp="onchange=\"info.loadInfo(1)\""/>
								</div>
								<input type="text" class="form-control input-sm w-120" id="title" placeholder="根据推送标题搜索" value="">
							  	<button type="button" class="btn btn-sm btn-default enter-fn" onclick="info.loadInfo(1)">查询</button>
							</div>
							<div class="col-sm-6 text-right">
							  	<div class="btn-group">
							  		<a href="javascript:;" class="btn btn-success btn-sm" onclick="info.edit()">新增</a>
							  	</div>
							</div>
						</div>
				  	</div>
					<div id="infoPanel" class="table-panel"></div>
					<div id="infoPage" class="table-page-panel"></div>
				</div>
			</div>
		</div>
		<br clear="all">
	</div>
	<jsp:include page="/WEB-INF/view/inc/footer.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<jsp:include page="/WEB-INF/view/inc/utils/page.jsp"></jsp:include>
<script type="text/javascript">
var infoPage = undefined;
var info = {
		//获取用户信息
		loadInfo : function(page) {
			if(!infoPage) {
				infoPage = new Page('infoPage', info.loadInfo, 'infoPanel', 'infoPage');
				infoPage.beginString = ['<table class="table table-striped table-hover"><thead><tr class="info">',
				                         '<th>系统名称</th>',
				                         '<th>事件名称</th>',
				                         '<th>标题</th>',
				                         '<th>接收人</th>',
				                         '<th>创建时间</th>',
				                         '<th width="120">操作</th>',
				                         '</tr></thead><tbody>'].join('');
				infoPage.endString = '</tbody></table>';
			}
			if(page != undefined)
				infoPage.page = page;
			
			JUtil.ajax({
				url : '${webroot}/motPush/f-json/pageQuery',
				data : { page:infoPage.page, size:infoPage.size, sysNo:$('#sysNo').val(), title: $('#title').val() },
				beforeSend: function(){ infoPage.beforeSend('加载信息中...'); },
				error : function(json){ infoPage.error('加载信息出错了!'); },
				success : function(json){
					if(json.code === 0) {
						function getResult(obj) {
							return ['<tr>',
							    	'<td>',obj.sysName,'</td>',
							    	'<td>',obj.eventName,'</td>',
							    	'<td>',obj.title,'</td>',
							    	'<td>',obj.receUserIds,'</td>',
							    	'<td>',obj.createTime,'</td>',
							    	'<td>',
							    	'<a class="glyphicon text-success" href="javascript:info.look(\'',obj.mpId,'\')" title="查看记录">查看记录</a>',
							    	'</td>',
								'</tr>'].join('');
						}
						infoPage.operate(json.body, { resultFn:getResult, dataNull:'没有记录噢' });
					}
					else alert(JUtil.msg.ajaxErr);
				}
			});
		},
		edit : function(id) {
			dialog({
				title: '推送事件',
				url: webroot + '/motPush/f-view/edit?sysNo='+$('#sysNo').val(),
				type: 'iframe',
				width: 620,
				height: 450
			});
		},
		look : function(id) {
			dialog({
				title: '查看推送事件',
				url: webroot + '/motPush/f-view/look?mpId='+id,
				type: 'iframe',
				width: 620,
				height: 450
			});
		}
};
$(function() {
	info.loadInfo(1);
});
</script>
</body>
</html>