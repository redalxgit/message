<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/view/inc/sys.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-推送事件</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body class="cld-body">
	<div class="enter-panel ep-sm">
  		<div class="form-group">
			<label for="sysNo" class="col-sm-4">系统编码 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><my:select id="sysNo" items="${sysInfos}" headerKey="" headerValue="-- 请选择系统 --" cssCls="form-control" value="${param.sysNo}" exp="onchange=\"info.changeSys()\""/></div>
		</div>
		<div class="form-group">
			<label for="sysNo" class="col-sm-4">订阅事件 <span class="text-danger">*</span></label>
			<div class="col-sm-8">
				<select id="meId" class="form-control"></select>
			</div>
		</div>
  		<div class="form-group">
			<label for="title" class="col-sm-4">推送标题 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="title" placeholder="事件推送的标题" value=""></div>
		</div>
  		<div class="form-group">
			<label for="content" class="col-sm-4">推送内容</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="content" placeholder="事件推送的内容" value=""></div>
		</div>
  		<div class="form-group">
			<label for="receUserIds" class="col-sm-4">接收人 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="receUserIds" placeholder="事件的接收人(0代表所有人。多个;分隔)" value=""></div>
		</div>
  		<div class="form-group">
			<label for="receEmail" class="col-sm-4">接收邮箱</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="receEmail" placeholder="事件的接收邮箱" value=""></div>
		</div>
  		<div class="form-group">
			<label for="receSms" class="col-sm-4">接收手机</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="receSms" placeholder="事件的接收手机号" value=""></div>
		</div>
  		<div class="form-group">
			<label for="emailContent" class="col-sm-4">邮件内容</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="emailContent" placeholder="事件推送的邮件内容" value="${motPush.emailContent}"></div>
		</div>
  		<div class="form-group">
			<label for="emailFiles" class="col-sm-4">邮件附件</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="emailFiles" placeholder="事件推送的邮件附件" value="${motPush.emailFiles}"></div>
		</div>
  		<div class="form-group">
			<label for="smsContent" class="col-sm-4">短信内容</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="smsContent" placeholder="事件推送的短信内容" value="${motPush.smsContent}"></div>
		</div>
		
		
		<div class="form-table">
			<table class="table" id="dtlTable">
				<thead>
				<tr class="success">
					<th>类型 <span class="text-danger">*</span></th>
					<th>字段1 <span class="text-danger">*</span></th>
					<th>字段2</th>
					<th>字段3</th>
					<th>字段4</th>
					<th>字段5</th>
					<th>字段6</th>
					<th>字段7</th>
					<th>字段8</th>
					<th>操作</th>
				</tr>
				</thead>
				<tr id="dtlTableDataTemp" style="display: none;">
					<td><div class="form-group w-60">
						<input type="text" class="form-control input-sm" id="dtlTableType_idTemp" placeholder="类型" value="" data-validate="required">
						</div>
					</td>
					<td><div class="form-group w-60">
						<input type="text" class="form-control input-sm" id="dtlTableField1_idTemp" placeholder="字段1" value="" data-validate="required">
						</div>
					</td>
					<td><div class="form-group w-60">
						<input type="text" class="form-control input-sm" id="dtlTableField2_idTemp" placeholder="字段2" value="">
						</div>
					</td>
					<td><div class="form-group w-60">
						<input type="text" class="form-control input-sm" id="dtlTableField3_idTemp" placeholder="字段3" value="">
						</div>
					</td>
					<td><div class="form-group w-60">
						<input type="text" class="form-control input-sm" id="dtlTableField4_idTemp" placeholder="字段4" value="">
						</div>
					</td>
					<td><div class="form-group w-60">
						<input type="text" class="form-control input-sm" id="dtlTableField5_idTemp" placeholder="字段5" value="">
						</div>
					</td>
					<td><div class="form-group w-60">
						<input type="text" class="form-control input-sm" id="dtlTableField6_idTemp" placeholder="字段6" value="">
						</div>
					</td>
					<td><div class="form-group w-60">
						<input type="text" class="form-control input-sm" id="dtlTableField7_idTemp" placeholder="字段7" value="">
						</div>
					</td>
					<td><div class="form-group w-60">
						<input type="text" class="form-control input-sm" id="dtlTableField8_idTemp" placeholder="字段8" value="">
						</div>
					</td>
					<td><div class="form-group w-40">
						<a href="javascript:;" onclick="JUtil.biz.table.del('dtlTable', this)" class="dtlTableRowDelBtn">&nbsp;删除</a>
						</div>
					</td>
				</tr>
				<tbody id="dtlTableTbody">
				</tbody>
			</table>
		</div>
		<p class="text-center dtlTableAddTrBtn"><a href="javascript:JUtil.biz.table.add('dtlTable');" class="btn btn-link btn-sm">添加过滤条件</a></p>
  		
		
  		<div class="footer-operate">
			<span id="saveMsg" class="label label-danger"></span>
 			<div class="btn-group">
				<button type="button" id="saveBtn" class="btn btn-success enter-fn">保存</button>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<script type="text/javascript">
	var info = {
			changeSys: function() {
				var saveMsg = $('#saveMsg').empty();
				var meId = $('#meId').empty();
				meId.append('<option value="">-- 请选择事件 --</option>');
				var sysNo = $('#sysNo').val();
				if (JUtil.isEmpty(sysNo)) {
					//saveMsg.append('-- 请选择所属系统 --');
					return;
				}
				JUtil.ajax({
					url : '${webroot}/motEvent/f-json/findBySysNo',
					data : {
						sysNo: sysNo
					},
					success : function(json) {
						if (json.code === 0) {
							$.each(json.body, function(i, obj) {
								meId.append(['<option value="',obj.meId,'">',obj.name,'</option>'].join(''));
							});
							meId.val('${motPush.meId}');
						}
						else if (json.code === -1)
							saveMsg.append(JUtil.msg.ajaxErr);
						else
							saveMsg.append(json.message);
					}
				});
			}
	};
	$(function() {
		info.changeSys();
		$('#saveBtn').click(function() {
			var saveMsg = $('#saveMsg').empty();
			
			var sysNo = $('#sysNo');
			if(JUtil.isEmpty(sysNo.val())) {
				saveMsg.append('请选择系统');
				sysNo.focus();
				return;
			}
			var meId = $('#meId');
			if(JUtil.isEmpty(meId.val())) {
				saveMsg.append('请选择事件');
				meId.focus();
				return;
			}
			var title = $('#title');
			if(JUtil.isEmpty(title.val())) {
				saveMsg.append('请输入推送标题');
				title.focus();
				return;
			}
			var receUserIds = $('#receUserIds');
			if(JUtil.isEmpty(receUserIds.val())) {
				saveMsg.append('请输入事件的接收人');
				receUserIds.focus();
				return;
			}
			
			var dtls = [];
			//获取详情的信息
			$('#dtlTableTbody').find('tr').each(function(i, obj) {
				var dtl = {};
				$(obj).find('td').each(function(j, cld) {
					if(j == 0) {
						dtl['type'] = $(cld).find('input').val();
					} else if(j == 1) {
						dtl['field1'] = $(cld).find('input').val();
					} else if(j == 2) {
						dtl['field2'] = $(cld).find('input').val();
					} else if(j == 3) {
						dtl['field3'] = $(cld).find('input').val();
					} else if(j == 4) {
						dtl['field4'] = $(cld).find('input').val();
					} else if(j == 5) {
						dtl['field5'] = $(cld).find('input').val();
					} else if(j == 6) {
						dtl['field6'] = $(cld).find('input').val();
					} else if(j == 7) {
						dtl['field7'] = $(cld).find('input').val();
					} else if(j == 8) {
						dtl['field8'] = $(cld).find('input').val();
					}
				});
				dtls.push(dtl);
			});
			
			var saveBtn = $('#saveBtn');
			var orgVal = saveBtn.html();
			saveBtn.attr('disabled', 'disabled').html('保存中...');
			JUtil.ajax({
				url : '${webroot}/motPush/f-json/save',
				data : {
					sysNo: sysNo.val(),
					meId: meId.val(),
					title: title.val(),
					content: $('#content').val(),
					receUserIds: receUserIds.val(),
					emailContent: $('#emailContent').val(),
					emailFiles: $('#emailFiles').val(),
					smsContent: $('#smsContent').val(),
					receEmail: $('#receEmail').val(),
					receSms: $('#receSms').val(),
					filterString: JSON.stringify(dtls)
				},
				success : function(json) {
					if (json.code === 0) {
						saveMsg.attr('class', 'label label-success').append('保存成功');
						setTimeout(function() {
							parent.info.loadInfo();
							parent.dialog.close();
						}, 800);
					}
					else if (json.code === -1)
						saveMsg.append(JUtil.msg.ajaxErr);
					else
						saveMsg.append(json.message);
					saveBtn.removeAttr('disabled').html(orgVal);
				}
			});
		});
	});
	</script>
</body>
</html>