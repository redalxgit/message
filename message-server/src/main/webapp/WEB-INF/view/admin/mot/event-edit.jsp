<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/view/inc/sys.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-编辑事件</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body class="cld-body">
	<div class="enter-panel ep-sm">
		<input type="hidden" id="meId" value="${motEvent.meId}">
  		<div class="form-group">
			<label for="sysNo" class="col-sm-4">系统编码 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><my:select id="sysNo" items="${sysInfos}" headerKey="" headerValue="-- 请选择系统 --" cssCls="form-control" value="${motEvent.sysNo}"/></div>
		</div>
  		<div class="form-group">
			<label for="name" class="col-sm-4">名称 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="name" placeholder="事件名称" value="${motEvent.name}"></div>
		</div>
  		<div class="form-group">
			<label for="remark" class="col-sm-4">备注</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="remark" placeholder="事件备注信息" value="${motEvent.remark}"></div>
		</div>
		<div class="form-group" title="调度的加密方式">
			<label for="reqEncrypt" class="col-sm-4">加密方式 <span class="text-danger">*</span></label>
			<div class="col-sm-8">
				<my:radio name="reqEncrypt" dictcode="mot_event_reqEncrypt" defvalue="20" value="${motEvent.reqEncrypt}"/>
			</div>
		</div>
  		<div class="form-group">
			<label for="filterPlug" class="col-sm-4">过滤插件 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><my:select id="filterPlug" items="${plugins}" cssCls="form-control input-sm" value="${motEvent.filterPlug}"/></div>
		</div>
  		<div class="footer-operate">
			<span id="saveMsg" class="label label-danger"></span>
 			<div class="btn-group">
				<button type="button" id="saveBtn" class="btn btn-success enter-fn">保存</button>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<script type="text/javascript">
	$(function() {
		$('#saveBtn').click(function() {
			var saveMsg = $('#saveMsg').empty();
			
			var meId = $('#meId').val();
			var sysNo = $('#sysNo');
			if(JUtil.isEmpty(sysNo.val())) {
				saveMsg.append('请选择系统');
				sysNo.focus();
				return;
			}
			var name = $('#name');
			if(JUtil.isEmpty(name.val())) {
				saveMsg.append('请输入事件名称');
				name.focus();
				return;
			}

			var saveBtn = $('#saveBtn');
			var orgVal = saveBtn.html();
			saveBtn.attr('disabled', 'disabled').html('保存中...');
			JUtil.ajax({
				url : '${webroot}/motEvent/f-json/save',
				data : {
					meId: meId,
					sysNo: sysNo.val(),
					name: name.val(),
					remark: $('#remark').val(),
					reqEncrypt: $('input[name="reqEncrypt"]:checked').val(),
					filterPlug: $('#filterPlug').val()
				},
				success : function(json) {
					if (json.code === 0) {
						saveMsg.attr('class', 'label label-success').append('保存成功');
						setTimeout(function() {
							parent.info.loadInfo();
							parent.dialog.close();
						}, 800);
					}
					else if (json.code === -1)
						saveMsg.append(JUtil.msg.ajaxErr);
					else
						saveMsg.append(json.message);
					saveBtn.removeAttr('disabled').html(orgVal);
				}
			});
		});
	});
	</script>
</body>
</html>