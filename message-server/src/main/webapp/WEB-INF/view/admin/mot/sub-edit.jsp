<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/view/inc/sys.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-编辑订阅</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body class="cld-body">
	<div class="enter-panel ep-sm">
		<input type="hidden" id="msId" value="${motSub.msId}">
  		<div class="form-group">
			<label for="sysNo" class="col-sm-4">系统编码 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><my:select id="sysNo" items="${sysInfos}" headerKey="" headerValue="-- 请选择系统 --" cssCls="form-control" value="${motSub.sysNo}" exp="onchange=\"info.changeSys()\""/></div>
		</div>
		<div class="form-group">
			<label for="sysNo" class="col-sm-4">订阅事件 <span class="text-danger">*</span></label>
			<div class="col-sm-8">
				<select id="meId" class="form-control"></select>
			</div>
		</div>
  		<div class="form-group">
			<label for="subUserId" class="col-sm-4">订阅人 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="subUserId" placeholder="事件订阅人" value="${motSub.subUserId}"></div>
		</div>
  		<div class="form-group">
			<label for="receEmail" class="col-sm-4">接收邮箱</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="receEmail" placeholder="接收的邮箱，多个;分隔" value="${motSub.receEmail}"></div>
		</div>
  		<div class="form-group">
			<label for="receSms" class="col-sm-4">接收短信</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="receSms" placeholder="接收的短信手机号，多个;分隔" value="${motSub.receSms}"></div>
		</div>
  		<div class="footer-operate">
			<span id="saveMsg" class="label label-danger"></span>
 			<div class="btn-group">
				<button type="button" id="saveBtn" class="btn btn-success enter-fn">保存</button>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<script type="text/javascript">
	var info = {
			changeSys: function() {
				var saveMsg = $('#saveMsg').empty();
				var meId = $('#meId').empty();
				meId.append('<option value="">-- 请选择事件 --</option>');
				var sysNo = $('#sysNo').val();
				if (JUtil.isEmpty(sysNo)) {
					//saveMsg.append('-- 请选择所属系统 --');
					return;
				}
				JUtil.ajax({
					url : '${webroot}/motEvent/f-json/findBySysNo',
					data : {
						sysNo: sysNo
					},
					success : function(json) {
						if (json.code === 0) {
							$.each(json.body, function(i, obj) {
								meId.append(['<option value="',obj.meId,'">',obj.name,'</option>'].join(''));
							});
							meId.val('${motSub.meId}');
						}
						else if (json.code === -1)
							saveMsg.append(JUtil.msg.ajaxErr);
						else
							saveMsg.append(json.message);
					}
				});
			}
	};
	$(function() {
		info.changeSys();
		$('#saveBtn').click(function() {
			var saveMsg = $('#saveMsg').empty();
			
			var msId = $('#msId').val();
			var sysNo = $('#sysNo');
			if(JUtil.isEmpty(sysNo.val())) {
				saveMsg.append('请选择系统');
				sysNo.focus();
				return;
			}
			var meId = $('#meId');
			if(JUtil.isEmpty(meId.val())) {
				saveMsg.append('请选择事件');
				meId.focus();
				return;
			}
			var subUserId = $('#subUserId');
			if(JUtil.isEmpty(subUserId.val())) {
				saveMsg.append('请输入事件订阅人编号');
				subUserId.focus();
				return;
			}

			var saveBtn = $('#saveBtn');
			var orgVal = saveBtn.html();
			saveBtn.attr('disabled', 'disabled').html('保存中...');
			JUtil.ajax({
				url : '${webroot}/motSub/f-json/save',
				data : {
					msId: msId,
					sysNo: sysNo.val(),
					meId: meId.val(),
					subUserId: subUserId.val(),
					receEmail: $('#receEmail').val(),
					receSms: $('#receSms').val()
				},
				success : function(json) {
					if (json.code === 0) {
						saveMsg.attr('class', 'label label-success').append('保存成功');
						setTimeout(function() {
							parent.info.loadInfo();
							parent.dialog.close();
						}, 800);
					}
					else if (json.code === -1)
						saveMsg.append(JUtil.msg.ajaxErr);
					else
						saveMsg.append(json.message);
					saveBtn.removeAttr('disabled').html(orgVal);
				}
			});
		});
	});
	</script>
</body>
</html>