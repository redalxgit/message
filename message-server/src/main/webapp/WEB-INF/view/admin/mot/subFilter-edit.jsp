<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/view/inc/sys.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${projectName}-编辑过滤条件</title>
<jsp:include page="/WEB-INF/view/inc/css.jsp"></jsp:include>
</head>
<body class="cld-body">
	<div class="enter-panel ep-sm">
		<input type="hidden" id="msfId" value="${motSubFilter.msfId}">
		<input type="hidden" id="msId" value="${param.msId}">
  		<div class="form-group">
			<label for="type" class="col-sm-4">类型 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="type" placeholder="过滤业务类型" value="${motSubFilter.type}"></div>
		</div>
  		<div class="form-group">
			<label for="field1" class="col-sm-4">字段1 <span class="text-danger">*</span></label>
			<div class="col-sm-8"><input type="text" class="form-control" id="field1" placeholder="过滤数据1(设为0代表接收该类型所有通知)" value="${motSubFilter.field1}"></div>
		</div>
  		<div class="form-group">
			<label for="field2" class="col-sm-4">字段2</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="field2" placeholder="过滤数据2" value="${motSubFilter.field2}"></div>
		</div>
  		<div class="form-group">
			<label for="field3" class="col-sm-4">字段3</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="field3" placeholder="过滤数据3" value="${motSubFilter.field3}"></div>
		</div>
  		<div class="form-group">
			<label for="field4" class="col-sm-4">字段4</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="field4" placeholder="过滤数据4" value="${motSubFilter.field4}"></div>
		</div>
  		<div class="form-group">
			<label for="field5" class="col-sm-4">字段5</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="field5" placeholder="过滤数据5" value="${motSubFilter.field5}"></div>
		</div>
  		<div class="form-group">
			<label for="field6" class="col-sm-4">字段6</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="field6" placeholder="过滤数据6" value="${motSubFilter.field6}"></div>
		</div>
  		<div class="form-group">
			<label for="field7" class="col-sm-4">字段7</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="field7" placeholder="过滤数据7" value="${motSubFilter.field7}"></div>
		</div>
  		<div class="form-group">
			<label for="field8" class="col-sm-4">字段8</label>
			<div class="col-sm-8"><input type="text" class="form-control" id="field8" placeholder="过滤数据8" value="${motSubFilter.field8}"></div>
		</div>
  		<div class="footer-operate">
			<span id="saveMsg" class="label label-danger"></span>
 			<div class="btn-group">
				<button type="button" id="saveBtn" class="btn btn-success enter-fn">保存</button>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/view/inc/js.jsp"></jsp:include>
	<script type="text/javascript">
	$(function() {
		$('#saveBtn').click(function() {
			var saveMsg = $('#saveMsg').empty();
			
			var type = $('#type');
			if(JUtil.isEmpty(type.val())) {
				saveMsg.append('请输入过滤的类型');
				type.focus();
				return;
			}
			var field1 = $('#field1');
			if(JUtil.isEmpty(field1.val())) {
				saveMsg.append('请输入过滤数据1');
				field1.focus();
				return;
			}

			var saveBtn = $('#saveBtn');
			var orgVal = saveBtn.html();
			saveBtn.attr('disabled', 'disabled').html('保存中...');
			JUtil.ajax({
				url : '${webroot}/motSubFilter/f-json/save',
				data : {
					msfId: $('#msfId').val(),
					msId: $('#msId').val(),
					type: type.val(),
					field1: field1.val(),
					field2: $('#field2').val(),
					field3: $('#field3').val(),
					field4: $('#field4').val(),
					field5: $('#field5').val(),
					field6: $('#field6').val(),
					field7: $('#field7').val(),
					field8: $('#field8').val()
				},
				success : function(json) {
					if (json.code === 0) {
						saveMsg.attr('class', 'label label-success').append('保存成功');
						setTimeout(function() {
							parent.info.loadInfo();
							parent.dialog.close();
						}, 800);
					}
					else if (json.code === -1)
						saveMsg.append(JUtil.msg.ajaxErr);
					else
						saveMsg.append(json.message);
					saveBtn.removeAttr('disabled').html(orgVal);
				}
			});
		});
	});
	</script>
</body>
</html>